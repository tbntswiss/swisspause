$(document).ready(function(){

	var mixer = mixitup('.videos-container', {
		selectors: {
			target: '.video-item'
		},
		multifilter: {
			enable: true
		},
		animation: {
			"duration": 250,
			"nudge": false,
			"reverseOut": false,
			"effects": "fade"
		}
	});


});
