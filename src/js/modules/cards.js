'use strict';

var _Cards = (function()
{
	var el = {};

	var animationDuration = 10;

	// Init
	var init = function()
	{
		initDOM();

		if (el.cards_container.length === 0)
			return;

		initCardsAnimation();
	};

	// Init DOM
	var initDOM = function()
	{
		el = {};

		el.cards_container = $('[data-cards-container]');
		el.cards_columns = $('[data-cards-column]');
	};

	// Init animation
	var initCardsAnimation = function()
	{
		for (var i = 0, c = el.cards_columns.length; i < c; i++)
			startCardsAnimation(el.cards_columns.eq(i));
	};

	var startCardsAnimation = function($column)
	{
		var cardHeight = $column.find('[data-card-item]').eq(0).outerHeight();
		var cardTop = Math.abs($column.position().top);

		var tlCardsAnimation = new TimelineMax();

		if (cardHeight === cardTop) {
			tlCardsAnimation
				.set($column, { y: '0px' })
				.to($column, animationDuration, {
					y: (0 - cardHeight) + 'px',
					ease: 'linear',
					repeat: -1,
					onRepeat: function() {
						endCardsAnimation($(this.target));
					},
				});
		}
		else {
			var cardOffset = Math.abs(cardTop - cardHeight);
			var animationDurationLast = (cardOffset / cardHeight) * animationDuration;

			tlCardsAnimation
				.to($column, animationDurationLast, {
					y: (0 - cardHeight) + 'px',
					ease: 'linear',
					onComplete: function() {
						var $column = $(this.target);

						endCardsAnimation($column);
						startCardsAnimation($column);
					},
				});
		}
	};

	var endCardsAnimation = function($column)
	{
		$column.find('[data-card-item]').first().detach().appendTo($column);
	};

	$(document).ready(init);

	return {}
})();
