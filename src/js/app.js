'use strict';

$(document).ready(function()
{
	console.log('%c Made with love by TBNT - Lausanne, Switzerland', 'background: #202542; color: #EF604C;');
	console.log('%c Visit website http://tbnt.digital/', 'background: #202542; color: #EF604C;');


	setTimeout(function() {
		$('body').removeClass('load');
		$('body').addClass('loaded');
	}, 6000);

	setTimeout(function() {
		$('.loader').addClass('hidden');
	}, 6310);


	var mixer = mixitup('.videos-container', {
		selectors: {
			target: '.video-item'
		},
		multifilter: {
			enable: true
		},
		animation: {
			"duration": 250,
			"nudge": false,
			"reverseOut": false,
			"effects": "fade"
		}
	});

	$('[data-filters-close]').on('click', function(){
		TweenLite.to('.filters-panel', .4, {left: '-100vw'});
	});
	$('[data-filters-open]').on('click', function(){
		TweenLite.to('.filters-panel', .4, {left: 0});
	});

	var scrollDistance = $('[data-header]').height() + $('[data-navbar]').height()

	//console.log(scrollDistance);
	TweenLite.set('[data-filters-panel]', { top: scrollDistance})

	$(window).scroll(function(){
		if($(window).width() > 678){
			if ($(this).scrollTop() > scrollDistance) {
				TweenLite.set('[data-filters-panel]', { position: 'fixed', top: 0});
			} else {
				TweenLite.set('[data-filters-panel]', { position: 'absolute', top: scrollDistance});
			}
		}
	});
	//WOW
	var wow = new WOW()
	wow.init();


});
